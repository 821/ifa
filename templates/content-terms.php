<header>
    <h2>Términos y condiciones</h2>
</header>
<ul class="text">
    <li>La orden debe realizarse por lo menos un dia anterior al dia que se desean recibir los alimentos. El limite de horario es hasta las 13:00 hrs.</li>
    <li>Los alimentos son entregados un dia antes entre 13:00 y 19:00 hrs. Nosotros contactamos para acordar la entrega.</li>
    <li>La cancelación del servicio debe ser con dos días de anticipación.</li>
    <li>El envío será gratuito al consumir todos los tiempos de comida del dia, de no ser
        así, el costo es de 25 pesos extra.</li>
    <li>Si el pedido es de mayoreo (mas de 4 personas), el envío será gratuito,
        independientemente del numero de tiempos de comida</li>
    <li>El servicio debe liquidarse antes de la entrega por medio de tarjeta de crédito, debito, transferencia bancaria, paypal, Oxxo. De no realizar el pago, el servicio no se llevara a cabo.</li>
    <li>IFA SMART MEALS se deslinda de responsabilidades en cuanto a resultados y/o cambios causados por negligencia u omisión de las recomendaciones del o los nutriólogos y también por planes nutricionales expedidos por nutriólogos agenos a la empresa.</li>
    <li>No hay reembolsos en efectivo.</li>
</ul>
