<section class="about-us">
    <article>
        <img src="<?= bloginfo('template_directory') ?>/assets/images/nosotros-ifa.png" class="img-responsive ensalada-ifa">
        <h2>¿Quiénes somos?</h2>
        <p>Somos una empresa dedicada a la nutrición, mediante un sistema especializado en cubrir
        cualquier necesidad sobre alimentación.</p>
        <p>En IFA somos fanáticos de la salud, sabemos que para lograr tus metas primero
        debes estar bien alimentado.</p>
        <h3>Misión:</h3>
        <p>Superar las expectativas de nuestros clientes a través de las mejores soluciones en nutrición y
            alimentación de la mano del mejor equipo de profesionales, las mejores propuestas de valor y sobre
            todo, de la pasión por el éxito y el bienestar de las personas.</p>
        <h3 id="vision">Visión:</h3>
        <p>Posicionarnos a nivel nacional como la institución líder en nutrición y alimentación,
            con las mejores propuestas de valor alcanzando la satisfacción total de cada uno de nuestros
            clientes gracias a la amplia gama de soluciones ofertadas y creadas con base en las necesidades
            y deseos de nuestro mercado.</p>
    </article>
    <article>
        <img src="<?= bloginfo('template_directory') ?>/assets/images/nosotros-ensalada.jpg" class="img-responsive ensalada">
        <h2>¿Qué hacemos?</h2>
        <p class="text-justify">Hacemos la vida nutricional de las personas más práctica y sencilla, ya que diariamente
        enviamos a su domicilio, (hogar u oficina), los alimentos necesarios para llevar a cabo
        una dieta sin contratiempos, ya sea con el objetivo de bajar de peso o simplemente alimentarse bien.
        Además de enseñarles a balancear sus alimentos y mejorar su salud.</p>
        <h3>Valores:</h3>
        <p class="text-justify">
            <b>Respeto:</b> Hacia todas las personas que nos rodean y el medio en el que
            interactuamos <br /> <br />
            <b>Compromiso:</b> Asumiendo la obligación y responsabilidad que recae en lo que hacemos.
            Innovación: alcanzada a través de la mejora continua y la constante entrega de
            soluciones ante los retos que se nos presenten. <br /><br />
            <b>Trabajo en Equipo:</b> Trabajamos como una sola unidad con un mismo objetivo: el bienestar y la
            satisfacción de nuestros clientes. <br /> <br />
            <b>Calidad:</b> Hacemos bien todo lo que hacemos porque somos especialistas en ello. <br /><br />
            <b>Pasión:</b> La reflejamos en todo lo que hacemos porque lo que hacemos es lo que amamos.
        </p>
    </article>
</section>
