<div class="row">
    <header class="col-md-12">
        <h1><?php the_title() ?></h1>
    </header>
    <section class="col-md-3 blog--sidebar">
        <?php dynamic_sidebar('sidebar-primary') ?>
    </section>
    <section class="col-md-9 blog--posts">
        <?php query_posts('post_type=post&post_status=publish&posts_per_page=5&paged='. get_query_var('paged')); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article class="post">
                <header class="post--header">
                    <h2 class="post--title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                    <hr class="post--hr">
                </header>
                    <figure class="post--figure">
                        <?php if (has_post_thumbnail()): ?>
                            <?php the_post_thumbnail( 'medium', array( 'class' => 'post--featured-image' ) ); ?>
                        <?php else: ?>
                            <img class="post--featured-image" src="<?= bloginfo('template_directory') ?>/assets/images/blog-default.jpg" />
                        <?php endif; ?>
                    </figure>
                <div class="post--content">
                    <?php the_excerpt() ?>
                </div>
            </article>
        <?php endwhile; ?>
        <nav class="blog--navigation">
            <?php
                the_posts_pagination( array(
                    'prev_text' => '&laquo;',
                    'next_text' => '&raquo;',
                ) );
            ?>
        </nav>
        <?php else: ?>
            <?php echo "No se encontraron entradas para tu solicitud."; ?>
        <?php endif; wp_reset_query(); ?>
    </section>
</div>
