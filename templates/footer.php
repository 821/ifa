<script src="https://cdnjs.cloudflare.com/ajax/libs/mixitup/2.1.11/jquery.mixitup.min.js"></script>
<footer class="content-info" role="contentinfo">
    <div class="footer-top"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-centered">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Cliente <span>Info</span></h2>
                        <ul>
                            <li><a href="<?php bloginfo('url'); ?>/my-account">Mi cuenta</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/cart">Mi carrito</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/checkout">Checkout</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h2>Sobre <span>Nosotros</span></h2>
                        <ul>
                            <li><a href="<?php bloginfo('url'); ?>/nosotros">Nosotros</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/faq">FAQ</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/contacto">Contacto</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/terminos-y-condiciones">Terminos y condiciones</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h2>Suscribete</h2>
                        <div id="mc_embed_signup">
                        <form action="//ifasmartmeals.us12.list-manage.com/subscribe/post?u=066ea469c2bf7dad78866abd0&amp;id=81dc99ca90" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">

                        <div class="mc-field-group">
                        	<input type="email" placeholder="correo@ejemplo.com" class="form-control" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                        </div>
                        	<div id="mce-responses" class="clear">
                        		<div class="response" id="mce-error-response" style="display:none"></div>
                        		<div class="response" id="mce-success-response" style="display:none"></div>
                        	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;"><input type="text" name="b_066ea469c2bf7dad78866abd0_81dc99ca90" tabindex="-1" value=""></div>
                            <div class="clear"><input type="submit" class="btn btn-red" value="Suscribirse" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                            </div>
                        </form>
                        </div>

                        <!--End mc_embed_signup-->
                    </div>
                </div>
            </div>
        </div> <!-- ./row -->
    </div> <!-- ./container -->
</footer>
