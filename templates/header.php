<header class="banner" role="banner">
    <nav class="secundary-menu">
        <div class="btn-group" role="group">
            <a href="#" class="btn btn-social" role="button"><i class="fa fa-facebook"></i></a>
            <a href="#" class="btn btn-social" role="button"><i class="fa fa-instagram"></i></a>
            <a href="<?php bloginfo('url'); ?>/my-account" class="btn btn-secondary-nav" role="button"><span>Mi Cuenta</span></a>
            <a href="<?php bloginfo('url'); ?>/cart" class="btn btn-secondary-nav" role="button"><span><i class="fa fa-shopping-cart"></i> Mi Carrito</span></a>
            <a href="<?php bloginfo('url'); ?>/checkout" class="btn btn-secondary-nav" role="button"><span>Checkout</span></a>
        </div>
    </nav>
    <div class="container">
        <a href="<?= bloginfo('url'); ?>" title="Inicio">
            <img class="logo" src="<?= bloginfo('template_directory') ?>/assets/images/logo.png" />
        </a>
    </div>
    <nav class="primary-menu">
        <div class="container">
            <div class="btn-group" role="group">
                <a href="/" class="btn btn-primary-nav" role="button"><span>Menu</span></a>
                <a href="/producto/platillo-personalizado" class="btn btn-primary-nav" role="button"><span>Platillo personzalido</span></a>
                <a href="/envianos-tu-dieta" class="btn btn-primary-nav" role="button"><span>Envíanos tu dieta</span></a>
                <a href="/blog" class="btn btn-primary-nav" role="button"><span>Blog</span></a>
                <a href="/contacto" class="btn btn-primary-nav" role="button"><span>Contacto</span></a>
            </div>
        </div>
    </nav>
</header>
