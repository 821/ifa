<div class="row">
    <div class="col-md-12">
        <header>
            <h2><?php the_title() ?></h2>
        </header>
    </div>
    <div class="col-md-6">
        <?php echo do_shortcode('[contact-form-7 id="92" title="Envia tu dieta"]') ?>
    </div>
    <div class="col-md-6">
        <div class="row">
            <img src="<?= bloginfo('template_directory') ?>/assets/images/step1.png" class="alignleft" />
            <h3>Recibimos tu dieta</h3>
        </div>
        <div class="row">
            <img src="<?= bloginfo('template_directory') ?>/assets/images/step2.png" class="alignleft" />
            <h3>Nuestros expertos la analizan y se comunican contigo</h3>
        </div>
        <div class="row">
            <img src="<?= bloginfo('template_directory') ?>/assets/images/step3.png" class="alignleft" />
            <h3>Disfruta de IFA Smart Meals en la puerta de tu hogar</h3>
        </div>
    </div>
</div>
