<section class="contact">
    <img class="contact-background" src="<?= bloginfo('template_directory') ?>/assets/images/platillo.png" alt="Pasta verde" />
    <header>
        <h2>Contacto</h2>
    </header>
    <div class="row">
        <div class="col-md-7">
            <h3>Formulario de contacto</h3>
            <?php
                if( function_exists( 'ninja_forms_display_form' ) ) {
                    ninja_forms_display_form( 1 );
                }
            ?>
        </div>
        <div class="col-md-5">
            <h3>Detalles de contacto</h3>
            <p class="text">
                <b>Dirección:</b> Direccion #123. Guadalajara, Jalisco. <br />
                <b>Correo:</b> yo@ejemplo.com <br />
                <b>Teléfono:</b> (33) 1522-3173
            </p>
        </div>
    </div>
</section>
