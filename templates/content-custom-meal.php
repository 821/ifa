<section>
    <article class="row">
        <header>
            <h2><?php the_title(); ?></h2>
            <h4>Elige uno de cada uno</h4>
        </header>
        <?php if (isset($_POST['nombre'])): ?>
            <?php echo $_POST['nombre']; ?>
        <?php endif; ?>
        <form action="<?php the_permalink() ?>" method="post">
            <input type="text" name="nombre">
            <input type="submit" class="btn btn-red" value="Enviar">
        </form>
    </article>
</section>
