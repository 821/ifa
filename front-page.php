<?php
    $args = array(
                'post_type' => 'product',
                'posts_per_page' => 6,
                'tax_query' => array(
            		'relation' => 'OR',
            		array(
            			'taxonomy' => 'product_cat',
            			'field' => 'slug',
            			'terms' => 'lunes'
            		),
            		array(
            			'taxonomy' => 'product_cat',
            			'field' => 'slug',
            			'terms' => 'martes'
            		),
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => 'miercoles'
                    ),
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => 'jueves'
                    ),
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => 'viernes'
                    )
            	)
            );
    $loop = new WP_Query( $args );
?>
<div class="front-page">
    <?php putRevSlider( 'front-slider' ); ?>
    <section class="menu">
        <div class="container">
            <header class="row text-center">
                <h1>Menu</h1>
            </header>
            <div class="row text-center">
                <nav class="menu-navigation filter-button-group">
                    <button data-filter=".Lunes" class="filter btn btn-default" role="button">Lunes</button>
                    <button data-filter=".Martes" class="filter btn btn-default" role="button">Martes</button>
                    <button data-filter=".Miércoles" class="filter btn btn-default" role="button">Miércoles</button>
                    <button data-filter=".Jueves" class="filter btn btn-default" role="button">Jueves</button>
                    <button data-filter=".Viernes" class="filter btn btn-default" role="button">Viernes</button>
                </nav>
            </div>
            <div class="row">
                <div class="card-group center-block" id="js-mix-container">
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                        <?php
                            $terms = wp_get_post_terms($loop->post->ID, 'product_cat');
                        ?>
                        <div class="card mix <?= $terms[0]->name ?>">
                            <?php if (has_post_thumbnail( $loop->post->ID )): ?>
                                <a href="<?= get_permalink( $loop->post->ID ) ?>">
                                    <?php
                                        the_post_thumbnail( 'medium', array( 'class' => 'card-img' ) );
                                    ?>
                                </a>
                            <?php else: ?>

                            <?php endif; ?>
                            <div class="card-content">
                                <div class="card-title">
                                    <a class="card-title-link" href="<?= get_permalink( $loop->post->ID ) ?>">
                                        <?php
                                            the_title();
                                        ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div> <!-- ./card-group -->
                <?php wp_reset_query(); ?>
            </div> <!-- ./row -->
        </div> <!-- -./container -->
    </section><!-- menu -->
</div>
<section class="blog-front">
    <h1 class="blog-title"><a href="/blog">BLOG</a></h1>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-centered">
                <?php query_posts('post_type=post&post_status=publish&posts_per_page=2&paged='. get_query_var('paged')); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="post row">
                        <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                        <hr class="post-separator">
                        <div class="media">
                            <?php if (has_post_thumbnail()): ?>
                                <div class="media-left">
                                    <?php the_post_thumbnail( 'medium', array( 'class' => 'media-object post-image' ) ); ?>
                                </div>
                            <?php endif; ?>
                            <div class="post-content text-justify media-body">
                                <?php the_excerpt() ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                    <!-- post navigation -->
                <?php else: ?>
                    Aún no se han publicado posts.
                <?php endif; ?>

            </div> <!-- ./col-centered -->
        </div> <!-- ./row -->
    </div> <!-- ./container -->
</section> <!-- ./blog -->
