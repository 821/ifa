<?php
/**
 * Template Name: Platillo personalizado
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'custom-meal'); ?>
<?php endwhile; ?>
